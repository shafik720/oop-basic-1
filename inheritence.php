<?php
class other{
    public $name = "Shafiqul Hasan Rasel";
    public $age = 22 ;

    public function details(){
        echo " Name: {$this->name} <br> Age: {$this->age}";
    }
}

class another extends other{

    public $name = "Abir Rahman";
    public $level = "admin";

    public function details(){
        echo " Name: {$this->name} <br> Age: {$this->age} <br> Level: {$this->level}";
    }

}

$user = new another();
$user->details();
echo "<br>".$user->age;
?>